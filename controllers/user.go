package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/ayush-iitkgp/blocksi/utils"

	"gitlab.com/ayush-iitkgp/blocksi/models"

	jwt "github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

// ErrorResponse blah
type ErrorResponse struct {
	Err string
}

type error interface {
	Error() string
}

var db = utils.ConnectDB()

// TestAPI tests if API is up and running
func TestAPI(w http.ResponseWriter, r *http.Request) {
	// http.ServeFile(w, r, "templates/index.html")
	w.Write([]byte("API is working"))
}

//RegisterUser function -- create a new user
func RegisterUser(w http.ResponseWriter, r *http.Request) {

	user := &models.User{}
	// fmt.Printf(r.Body)
	json.NewDecoder(r.Body).Decode(user)

	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
		err := ErrorResponse{
			Err: "Password Encryption  failed",
		}
		json.NewEncoder(w).Encode(err)
	}

	user.Password = string(pass)

	createdUser := db.Create(user)
	var errMessage = createdUser.Error

	if createdUser.Error != nil {
		fmt.Println(errMessage)
		http.Error(w, "Server error, unable to create your account.", 500)
	}
	// http.Redirect(w, r, "/", 301)
	expiresAt := time.Now().Add(time.Minute * 100).Unix()

	tk := &models.Token{
		Username: user.Username,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	tokenString, error := token.SignedString([]byte("secret"))
	if error != nil {
		fmt.Println(error)
	}
	var resp = map[string]interface{}{"status": true, "message": "User successfully created"}
	resp["token"] = tokenString //Store the token in the response
	resp["user"] = createdUser
	json.NewEncoder(w).Encode(resp)
}

// Login handler logins a particular user
func Login(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		var resp = map[string]interface{}{"status": false, "message": "Invalid request"}
		json.NewEncoder(w).Encode(resp)
		http.Redirect(w, r, "/login", 301)
		return
	}
	resp := FindOne(user.Username, user.Password)
	json.NewEncoder(w).Encode(resp)
}

// FindOne
func FindOne(username, password string) map[string]interface{} {
	user := &models.User{}

	if err := db.Where("Username = ?", username).First(user).Error; err != nil {
		var resp = map[string]interface{}{"status": false, "message": "Username not found"}
		return resp
	}
	expiresAt := time.Now().Add(time.Minute * 100).Unix()

	errf := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if errf != nil && errf == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
		var resp = map[string]interface{}{"status": false, "message": "Invalid login credentials. Please try again"}
		return resp
	}

	tk := &models.Token{
		Username: user.Username,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	tokenString, error := token.SignedString([]byte("secret"))
	if error != nil {
		fmt.Println(error)
	}

	var resp = map[string]interface{}{"status": true, "message": "logged in"}
	resp["token"] = tokenString //Store the token in the response
	resp["user"] = user
	return resp
}
